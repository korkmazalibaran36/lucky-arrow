﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnketOynat : MonoBehaviour
{

#if UNITY_ANDROID || UNITY_IPHONE

    private string apiKey;
    public int i;
    private Position pollfishPosition = Position.TOP_LEFT;
    public Rect rectmine;
    private bool releaseMode = false;
    private bool offerwallMode = false;
    private bool rewardMode = true;
    private int indPadding = 0;
    private string requestUUID = "my_id";
    private bool ispaused = false;

    private string[] text = new string[] { "Rewarded Surveys" };

    private static int curGridInt;
    private static int oldGridInt = -1;

    private GUIStyle buttonStyle;
    private GUIStyle labelStyle;

    private int currentScreenOrientation = 0;

    void OnApplicationPause(bool pause)
    {

        Debug.Log("PollfishDemo - OnApplicationPaused: " + pause);

        if (pause)
        {
            // we are in background
            ispaused = true;

        }
        else
        {
            // we are in foreground again.

            ispaused = false;

        }

    }


    public void Update()
    {
        /* handling Android back event */

        if (Input.GetKeyDown(KeyCode.Escape))
        {

            Pollfish.ShouldQuit();
        }

        if (!ispaused)
        { // resume

            Time.timeScale = 1;

        }
        else if (ispaused)
        { // pause

            Time.timeScale = 0;
        }



        // init Pollfish on orientation change in case you support different screen orientations in your app

        if (currentScreenOrientation != (int)Screen.orientation)
        {

            currentScreenOrientation = (int)Screen.orientation;
            initializePollfish();

        }




    }

    public void OnEnable()
    {
        Debug.Log("PollfishDemo - onEnabled called");

#if UNITY_ANDROID

        //apiKey = "ANDROID_API_KEY";

        apiKey = "0797979b-62be-459d-a96a-4b17f5b3b72f";

#elif UNITY_IPHONE

		//apiKey = "IOS_API_KEY";
		
		apiKey = "1603299a-ec6a-464d-954c-538f3669571e";
				
#endif


    }


    void OnGUI ()
	{	

		buttonStyle = new GUIStyle(GUI.skin.button);
		buttonStyle.fontSize = 33;

		labelStyle = new GUIStyle ();

		labelStyle.fontSize = 18;
		labelStyle.normal.textColor = Color.black;
		labelStyle.alignment = TextAnchor.MiddleCenter;


		curGridInt = GUI.SelectionGrid(new Rect(0, Screen.height-100, Screen.width - 10000, 0), curGridInt, text, 3,buttonStyle);

        // uncomment to view show or hide buttons




        //rewarded mode (both Rewarded Survey & Offerwall approach)

     if ((!PollfishEventListener.isSurveyCompleted() && Pollfish.IsPollfishPresent() && (curGridInt == 1)) || (Pollfish.IsPollfishPresent() && (curGridInt == 2))) {

            if (GUI.Button(new Rect(10, 260, Screen.width - 20, 60), (curGridInt == 1) ? "Complete survey to win coins" : "Survey Offerwall", buttonStyle)) {

                Pollfish.ShowPollfish();
            }
            

            //rewarded mode after completion of survey
        }
      
        else if ((curGridInt == 1) && (PollfishEventListener.isSurveyRejected()))
        {

            labelStyle.fontSize = 32;
            labelStyle.normal.textColor = Color.black;
            labelStyle.alignment = TextAnchor.MiddleCenter;

            GUI.Label(new Rect(10, 260, Screen.width - 20, 100), "YOU REJECTED SURVEY", labelStyle);

        }
        else if ((curGridInt == 0) && (PollfishEventListener.isSurveyCompleted())) {

            labelStyle.fontSize = 32;
            labelStyle.normal.textColor = Color.black;
            labelStyle.alignment = TextAnchor.MiddleCenter;
            if(i == 0)
            {
                int a = PlayerPrefs.GetInt("coin");
                a += 100;
                PlayerPrefs.SetInt("coin", a);
                i = 1;
            }
            GUI.Label(new Rect(10, 260, Screen.width - 20, 100), "YOU WON COINS", labelStyle);
            //Pollfish.HidePollfish();
           
        }

        else if(curGridInt==0){
			GUI.Label (new Rect (Screen.width / 2 - 200, 110, 400, 40), "Rewarded Survey", labelStyle);
		}

        labelStyle.fontSize = 30;
        labelStyle.normal.textColor = Color.red;
        labelStyle.alignment = TextAnchor.MiddleCenter;

        // standard or rewarded mode of demo scene


        if (GUI.changed)
		{
			if (oldGridInt != curGridInt)
			{
				Debug.Log ("User changed mode");

				oldGridInt = curGridInt;
			
				initializePollfish ();
			}
		}
	}


    public void Anket()
    {
        initializePollfish();
        Debug.Log("PollfishDemo: Show Pollfish Button pressed.");

        Pollfish.ShowPollfish();
    }
        void initializePollfish()
        {



            // Send user demographic attributes to shorten or skip demographic surveys

            Dictionary<string, string> dict = new Dictionary<string, string>();
            //used in demographic surveys
            dict.Add("gender", "1");
            dict.Add("year_of_birth", "1974");
            dict.Add("marital_status", "2");
            dict.Add("parental", "3");
            dict.Add("education", "1");
            dict.Add("employment", "1");
            dict.Add("career", "2");
            dict.Add("race", "3");
            dict.Add("income", "1");
            //general user attributes
            dict.Add("email", "user_email@gmail.com");
            dict.Add("google_id", "USER_GOOGLE");
            dict.Add("linkedin_id", "USER_LINKEDIN");
            dict.Add("twitter_id", "USER_TWITTER");
            dict.Add("facebook_id", "USER_FB");
            dict.Add("phone", "USER_PHONE");
            dict.Add("name", "USER_NAME");
            dict.Add("surname", "USER_SURNAME");


            rewardMode = (curGridInt == 0) ? false : true;
            offerwallMode = (curGridInt == 2) ? true : false;

            Debug.Log("PollfishDemo||| - offerwallMode: " + offerwallMode);


            PollfishEventListener.resetStatus(); // using this for demo purposes

            Pollfish.PollfishParams pollfishParams = new Pollfish.PollfishParams();

            pollfishParams.OfferwallMode(offerwallMode);
            pollfishParams.IndicatorPadding(indPadding);
            pollfishParams.ReleaseMode(releaseMode);
            pollfishParams.RewardMode(rewardMode);
          pollfishParams.IndicatorPosition((int)pollfishPosition);
            pollfishParams.RequestUUID(requestUUID);
            pollfishParams.UserAttributes(dict);

            Pollfish.PollfishInitFunction(apiKey, pollfishParams);
        }


#endif
    public void hide()
    {
        Pollfish.HidePollfish();
    }

    }


