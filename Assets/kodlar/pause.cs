﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pause : MonoBehaviour {

    public GameObject home, tekrar, devam, sesiac, sesikapat;
    public Button pausebutton;
    public int ses;
     void Start()
    {
        ses = PlayerPrefs.GetInt("ses");
    }

    public void pausebut () {
        pausebutton.GetComponent<Image>().enabled = false;
        pausebutton.GetComponent<Button>().interactable = false;
        home.gameObject.SetActive(true);
        tekrar.gameObject.SetActive(true);
        devam.gameObject.SetActive(true);
        Time.timeScale = 0;
        if (ses == 0)
        {
            sesiac.gameObject.SetActive(false);
            sesikapat.gameObject.SetActive(true);
        }
        else if (ses == 1)
        {
            sesiac.gameObject.SetActive(true);
            sesikapat.gameObject.SetActive(false);
        }

    }

    public void sesac()
    {

        ses = 0;
        PlayerPrefs.SetInt("ses", ses);
        sesiac.gameObject.SetActive(false);
        sesikapat.gameObject.SetActive(true);
    }
    public void seskapa()
    {
        ses = 1;
        PlayerPrefs.SetInt("ses", ses);
        sesiac.gameObject.SetActive(true);
        sesikapat.gameObject.SetActive(false);
    }
    public void devamet()
    {
        pausebutton.GetComponent<Image>().enabled = true;
        pausebutton.GetComponent<Button>().interactable = true;
        sesiac.gameObject.SetActive(false);
        sesikapat.gameObject.SetActive(false);
        home.gameObject.SetActive(false);
        tekrar.gameObject.SetActive(false);
        devam.gameObject.SetActive(false);
        Time.timeScale = 1;

    }
    

}
