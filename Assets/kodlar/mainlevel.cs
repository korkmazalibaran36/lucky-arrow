﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mainlevel : MonoBehaviour {

    public int level,bestscore,levelextra;
    public Text txt,besttxt;

	void Update () {
        level = PlayerPrefs.GetInt("level");
        levelextra = level + 1;
        txt.text = levelextra.ToString();
        bestscore = PlayerPrefs.GetInt("bestscore");
        besttxt.text = bestscore.ToString();
    }
}
