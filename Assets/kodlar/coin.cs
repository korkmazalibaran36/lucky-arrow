﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class coin : MonoBehaviour {

    public AudioSource sescoin;
    public Text paratxt;
    public int  level,coina,levelextra;

    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        level = PlayerPrefs.GetInt("level");
        coina = PlayerPrefs.GetInt("coin");
        paratxt.text = coina.ToString();
        levelextra = level + 1;
      
	}
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "karakter")
        {
            sescoin.Play();
            coina  = coina + levelextra;
            PlayerPrefs.SetInt("coin",coina);
            this.gameObject.SetActive(false);
        }
    }
}
