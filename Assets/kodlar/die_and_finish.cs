﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class die_and_finish : MonoBehaviour {

    public int a, b,level,best,score,levelextra;
    public Text textlevel,bestscoretxt;
    public GameObject buton,buton2,text,text1,text2,reklam,bestscore,bestscoretxtgo,main,devam,ses1,ses2,pause;

    void Start () {
        score = PlayerPrefs.GetInt("score");
        level = PlayerPrefs.GetInt("level");
        levelextra = level + 1;

	}
	
	// Update is called once per frame
	void Update () {
        best = PlayerPrefs.GetInt("bestscore");
        PlayerPrefs.SetInt("level", level);
        textlevel.text = levelextra.ToString();
        bestscoretxt.text = best.ToString();


    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "hedef")
        {

            devam.gameObject.SetActive(false);
            pause.gameObject.SetActive(false);
            ses1.gameObject.SetActive(false);
            ses2.gameObject.SetActive(false);
            buton.gameObject.SetActive(true);
            bestscore.gameObject.SetActive(true);
            bestscoretxtgo.gameObject.SetActive(true);
            text.gameObject.SetActive(true);
            reklam.gameObject.SetActive(true);
            main.gameObject.SetActive(true);
            
            Time.timeScale = 0;
           
        }
        if (other.gameObject.name == "finish")
        {
            devam.gameObject.SetActive(false);
            pause.gameObject.SetActive(false);
            ses1.gameObject.SetActive(false);
            ses2.gameObject.SetActive(false);
            text2.gameObject.SetActive(true);
            buton2.gameObject.SetActive(true);
            bestscore.gameObject.SetActive(true);
            bestscoretxtgo.gameObject.SetActive(true);
            level += 1;
            PlayerPrefs.SetInt("level", level);
            Time.timeScale = 0;

        }
        if (other.gameObject.name == "out")
        {
            devam.gameObject.SetActive(false);
            pause.gameObject.SetActive(false);
            ses1.gameObject.SetActive(false);
            ses2.gameObject.SetActive(false);
            bestscore.gameObject.SetActive(true);
            bestscoretxtgo.gameObject.SetActive(true);
            buton.gameObject.SetActive(true);
            text1.gameObject.SetActive(true);
            main.gameObject.SetActive(true);
            Time.timeScale = 0;

        }
    }
}
