﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;


public class yeniac : MonoBehaviour {

    public GameObject siradaki;
    public AudioSource ses;
    public Animator animkarakter,animkarakter1;
    public Text scoretxt, bestscoretxt;
    public int score, bestscore,level,a,levelextra;


	void Start () {
        level = PlayerPrefs.GetInt("level");
        levelextra = level + 1;

    }

    void Update () {

        bestscore = PlayerPrefs.GetInt("bestscore");
        score = PlayerPrefs.GetInt("score");
        a = levelextra;
        scoretxt.text = score.ToString();
        bestscoretxt.text = bestscore.ToString();
        if (score > bestscore)
        {
            PlayerPrefs.SetInt("bestscore", score);
        }
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "karakter")
        {
           
            score = score + levelextra;
            PlayerPrefs.SetInt("score", score);
            siradaki.gameObject.SetActive(true);
            animkarakter.Play("karakter1");
            animkarakter1.Play("karakter2");
            ses.Play();

        }
       
    }
}
