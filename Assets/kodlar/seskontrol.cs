﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class seskontrol : MonoBehaviour {

    public AudioSource coin, music, passed,tiklama;
    public int ses;

	void Start () {

    }
	

	void Update () {
        ses = PlayerPrefs.GetInt("ses");

        if (ses == 1)
        {
            tiklama.volume = 0;
            coin.volume = 0;
            music.volume = 0;
            passed.volume = 0;
        }
        else
        {
            tiklama.volume = 1;
            coin.volume = 1;
            music.volume = 1;
            passed.volume = 1;
        }
     }
}
