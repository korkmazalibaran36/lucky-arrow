using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.iOS.Xcode;
#if UNITY_2017_1_OR_NEWER
using UnityEditor.iOS.Xcode.Extensions;
#endif

public class PollfishBuildPostprocessor : MonoBehaviour
{

 [PostProcessBuild]  
 public static void OnPostProcessBuild(BuildTarget buildTarget, string buildPath) {
    
      Debug.Log ("Post Processing IOS Build...");

  #if UNITY_IOS || UNITY_IPHONE

      Debug.Log ("UNITY_IOS");
      
      if (buildTarget != BuildTarget.iOS) return;

      var projPath = Path.Combine(buildPath, "Unity-iPhone.xcodeproj/project.pbxproj");
      var proj = new PBXProject();
      proj.ReadFromString(File.ReadAllText(projPath));


     // var target = proj.TargetGuidByName("Unity-iPhone");
      var target = GetSourcesTargetGuid(proj);

      var fileGuid = proj.FindFileGuidByProjectPath("Frameworks/Plugins/iOS/pollfish.framework");

      proj.RemoveFile(fileGuid); // to avoid adding to embedframeworks
 
       proj.AddFileToBuild (target, proj.AddFile(Application.dataPath + "/Plugins/iOS/pollfish.framework", "Frameworks/Plugins/iOS/pollfish.framework", PBXSourceTree.Source));


      #if UNITY_2017_1_OR_NEWER
        proj.AddFileToEmbedFrameworks(target, fileGuid);
      #endif

      proj.AddFrameworkToProject (target, "WebKit.framework", false);  
      proj.AddFrameworkToProject (target, "CoreTelephony.framework", false);  
      proj.AddFrameworkToProject (target, "SystemConfiguration.framework", false);  
      proj.AddFrameworkToProject (target, "AdSupport.framework", true);  
     
      proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");
      proj.AddBuildProperty(target, "GCC_ENABLE_OBJC_EXCEPTIONS", "YES");
      proj.AddBuildProperty(target, "CLANG_ENABLE_MODULES", "YES");


      File.WriteAllText(projPath, proj.WriteToString());

    #endif
  }

          static string GetSourcesTargetGuid(PBXProject project)
        {
#if UNITY_2019_3_OR_NEWER
            return project.GetUnityFrameworkTargetGuid();
#else
            return project.TargetGuidByName(PBXProject.GetUnityTargetName());
#endif
        }


  static string GetResourcesTargetGuid(PBXProject project)
  {
    #if UNITY_2019_3_OR_NEWER
       return project.GetUnityMainTargetGuid();
    #else
       return project.TargetGuidByName(PBXProject.GetUnityTargetName());
    #endif
  }
}